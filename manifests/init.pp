# @summary Installs the NVidia Drivers and CUDA.
#
# @param version
#   The version to install.
#
# @param cuda_version
#   If this parmeter is set, will install cuda at the specified parameter.
#   @example '11-6'
#
# @param bashrc_file_path
#   The path to the basrch file to update (uses concat).
#   If this parameter is set, will update the bashrc file with cuda environment variables.
#
class nvidia (
  String           $version,
  Optional[String] $cuda_version     = undef,
  Optional[String] $bashrc_file_path = undef,
) {
  class { 'nvidia::driver':
    version => $version,
  }
  contain nvidia::driver

  if $cuda_version {
    class { 'nvidia::cuda':
      version          => $cuda_version,
      bashrc_file_path => $bashrc_file_path,
      require          => Class['nvidia::driver'],
    }
    contain nvidia::cuda
  }
}
