# @summary Configures the xserver generating xorg.conf
#
# Note, only creates the configuration if GPUs are detected through a custom fact.
# Also, after this change, the system will need to be rebooted.
#
#
class nvidia::xconfig (
) {
  # Use custom fact to get GPUs and IDs.
  $ids = $facts['gpus_pci_ids']

  $gpu_size = size($ids)

  if $ids {
    file { '/etc/X11/xorg.conf':
      ensure  => file,
      content => epp('nvidia/xorg.conf.epp',
        {
          gpus         => size($ids),
          gpus_pci_ids => $ids
        }
      ),
    }
  }

  # fix for fan control
  case $facts['os']['release']['full'] {
    '22.04' : {
      $repo_version = 'ubuntu1804'

      file { '/etc/X11/Xwrapper.config':
        ensure => file,
      }

      file_line { 'fan_control_fix':
        path    => '/etc/X11/Xwrapper.config',
        line    => 'needs_root_rights=yes',
        require => File['/etc/X11/Xwrapper.config'],
      }
    }
    default : {
      # do nothing
    }
  }
}
