# @summary Installs the NVidia Driver.  Uses the graphics-driver ppa
#
# @param version
#   The version to install
#
class nvidia::driver (
  String $version,
) {
  include apt

  # setup the repository
  apt::ppa { 'ppa:graphics-drivers/ppa': }

  # install the driver
  package { "nvidia-driver-${version}":
    ensure  => latest,
    require => Apt::Ppa['ppa:graphics-drivers/ppa'],
  }
}
