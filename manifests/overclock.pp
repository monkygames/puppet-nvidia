# @summary creates a script for overclocking the gpus.
#
# @param user
#   The user to assign ownership over the settings.
#
# @param user_id
#   The id of the user to run the overclock script (requires sudo access).
#
# @param gpu_freq
#   Controls the GPUGraphicsClockOffset from nvidia-settings for the default GPU.
#
# @param vram_freq
#   Controls the GPUMemoryTransferRateOffset from nvidia-settings for each default GPU.
#   Note, GUI tools generally double the value you see when it is set via command line, so you may need to double this setting 
#   for the desired result.
#
# @param power_limit
#   Specifies maximum power limit in watts for the default GPU.
#
# @param fan_speed
#   The percentage to set the speed for all GPU Fans.
#
# @param gpu_size
#   The number of GPUs to control in the system.
#
# @param gpu_profiles
#   A hash with the name of the card specified with the following.
#   * 'id'            :
#       'card name'   :
#       'gpu_freq'    :
#       'vram_freq'   :
#       'power_limit' :
#
# @param enable_service
#   Add overclock as a service to ensure overclock is called on boot.
#
class nvidia::overclock (
  String         $user,
  Integer        $user_id,
  Integer        $gpu_freq,
  Integer        $vram_freq,
  Integer        $power_limit,
  Integer        $fan_speed,
  Integer        $gpu_size       = size($facts['gpus_pci_ids']),
  Optional[Hash] $gpu_profiles   = undef,
  Boolean        $enable_service = false,
) {
  file { '/usr/local/bin/overclock.bash':
    ensure  => file,
    owner   => $user,
    group   => $user,
    mode    => '0755',
    content => epp('nvidia/overclock.bash',{
        user_id      => $user_id,
        gpu_freq     => $gpu_freq,
        vram_freq    => $vram_freq,
        power_limit  => $power_limit,
        fan_speed    => $fan_speed,
        gpu_size     => $gpu_size,
        gpu_profiles => $gpu_profiles,
    }),
  }
  if $enable_service {
    systemd::unit_file { 'overclock.service':
      active    => true,
      enable    => true,
      content   => epp('nvidia/overclock.service.epp',
        {
          user => $user,
        }
      ),
      subscribe => File['/usr/local/bin/overclock.bash'],
    }
  }
}
