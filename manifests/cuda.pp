# @summary Installs nvidia's CUDA driver
#
# @param version
#   The version of CUDA to install.
#
# @param bashrc_file_path
#   The path to the basrch file to update (uses concat).
#   If this parameter is set, will update the bashrc file with cuda environment variables.
#
class nvidia::cuda (
  String           $version,
  Optional[String] $bashrc_file_path = undef,
) {
  case $facts['os']['release']['full'] {
    '18.04' : {
      $repo_version = 'ubuntu1804'
      $install_libcudnn = true
    }
    '20.04', '21.04' , '21.10' : {
      $repo_version = 'ubuntu2004'
      $install_libcudnn = true
    }
    '22.04' : {
      $repo_version = 'ubuntu2204'
      $install_libcudnn = false
    }
    default : {
      fail("${facts['os']['release']['full']} unsupported")
    }
  }

  # gpg keys can be found here:
  # https://developer.download.nvidia.com/compute/cuda/repos/${DISTRO}/${ARCHITECTURE}/7fa2af80.pub 
  # download keys and run the following command to get the fingerprint
  # gpg --show-keys

  include apt

  #$gpg_file = '/etc/apt/trusted.gpg.d/cuda.pub'
  #file{$gpg_file:
  #  ensure => file,
  #  source => "https://developer.download.nvidia.com/compute/cuda/repos/${repo_version}/x86_64/3bf863cc.pub",
  #}

  apt::source { 'cuda':
    comment  => 'A repository for nvidia cuda',
    key      => {
      id     => 'EB693B3035CD5710E231E123A4B469963BF863CC',
      server => "https://developer.download.nvidia.com/compute/cuda/repos/${repo_version}/x86_64/3bf863cc.pub",
    },
    location => "http://developer.download.nvidia.com/compute/cuda/repos/${repo_version}/x86_64/",
    repos    => '/',
    release  => '',
    include  => {
      src => false,
      deb => true,
    },
  }

  exec { 'apt_cuda_update':
    command     => '/usr/bin/apt update',
    refreshonly => true,
    subscribe   => Apt::Source['cuda'],
  }

  package { "cuda-toolkit-${version}":
    ensure  => latest,
    require => Exec['apt_cuda_update'],
  }

  if $install_libcudnn {
    package { 'libcudnn8':
      ensure  => latest,
      require => Package["cuda-toolkit-${version}"],
    }
  }

  if $bashrc_file_path {
    $version_major = split($version,'-')
    concat::fragment { 'cuda_bashrc':
      target  => $bashrc_file_path,
      content => epp('nvidia/cuda_bashrc.epp',{
          version => $version_major[0],
      }),
      require => Package["cuda-toolkit-${version}"],
    }
  }
}
