# NVIDIA Puppet Module Changelog

## 2022-12-22 - Release 0.6.2

- Removed debugging code.
- Updated stdlib and apt versions as was causing Issue #3.

## 2022-12-19 - Release 0.6.1

- Fixed a bug of assigning profiles in the overclock script.
- Fixed an issue with Xorg config causing X to fail with greater than 4 GPUs.

## 2022-12-16 - Release 0.6.0

- Added GPU profiles for overclocking based on the name of the GPU.
- Breaking parameter changes due to new profile settings.

## 2022-12-13 - Release 0.5.14

- Fixed Overclock service to run after X Starts.

## 2022-12-06 - Release 0.5.13

- Updated overclock to use FAN veriable instead of puppet template assigning in the loop.

## 2022-11-15 - Release 0.5.12

- Fixed multiple GPU X configuration bug.

## 2022-11-14 - Release 0.5.11

- Fixed syntax in overclock service file.

## 2022-11-14 - Release 0.5.10

- Set overclock service to be enabled on restart.

## 2022-11-08 - Release 0.5.9

- Added file management for xwrapper.conf.

## 2022-08-04 - Release 0.5.8

- Fixed fan control script.

## 2022-06-06 - Release 0.5.7

- Updated for new puppet linting.
- Switched to using $facts variable.

## 2022-05-24 - Release 0.5.6

- Added Ubuntu 22.04 support.
- Applied a fix for fan control not working in Ubuntu 22.04.
  Requires a system reboot after application.
- Added hiera parameter defaults.
- Changed the default version of cuda to 11-7 (where possible)
- Changed the default version of the nvidia driver to 510 (where possible).

## 2022-05-11 - Release 0.5.5

- Refreshed apt after setting up apt keys for cuda.

## 2022-05-06 - Release 0.5.4

- Updated Cuda Public Key as NVidia updated key in April 2022.

## 2022-01-11 - Release 0.5.3

- Added xorg configuration management.
- Added overclocking management.
- Added stdlib and systemd as a depedency.
- Added Ubuntu 18.04 Support.
- Added functional testing.

## 2021-12-21 - Release 0.5.2

- Fixed the default version for cuda.
- Added a license file.

## 2021-12-14 - Release 0.5.1

- Added REFERENCES.md

## 2021-12-13 - Release 0.5.0

- Initial Release.
