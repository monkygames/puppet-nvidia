# NVIDIA

## Description

Installs the proprietary NVIDIA drivers using the default-graphics PPA.  Also can install CUDA if requested.

## Cuda Additional Setup

If CUDA was installed, users of CUDA will need to setup their .bashrc file.  The following needs to be added to the user's .bashrc file assuming CUDA 11-1 was installed.

```bash
# set PATH for cuda 11.1 installation
if [ -d "/usr/local/cuda-11.1/bin/" ]; then
    export PATH=/usr/local/cuda-11.1/bin${PATH:+:${PATH}}
    export LD_LIBRARY_PATH=/usr/local/cuda-11.1/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
fi
```

I recommend using the [puppetlabs-account](https://forge.puppet.com/modules/puppetlabs/accounts) module for managing this.  Your puppet code can
manage /etc/bashrc.puppet with the above (using variables for the cuda-version).

Test the cuda setup with the following command:

```bash
nvcc  --version
```

## xorg configuration

This module can manage the xorg configuration file.  The configuration will automatically detect all NVIDIA graphics cards and will add
them to the xorg configuration as virtual monitors (headless mode). This also enables overclocking settings that can be configured either via command line or through the nvidia settings.

This is very useful for GPUs running in headless mode.

Use the following code to enable xconfig management.

```puppet
contain nvidia::xconfig
```

## Overclocking

This module provides a script and service for overclocking NVIDIA GPUs.  The nvidia::overclock class sets the following GPU settings for defaults:

* Power Limit
* Core Clock
* Memory Frequency
* Fan Speed

GPU Profiles have been added to configure cards based on their product name.
To specify a profile, create a hash with an integer id and the product name that is returned by nvidia-smi.  To find the product name, nvidia settings GUI can be used, just look at the GPU and the 'Graphics Processor'.

Or from the command line:

```bash
nvidia-smi -i [CARD_ID] -q | grep 'Product Name'
```

The hash format is as below

```yaml
   'id'              :
       'card_name'   :
       'gpu_freq'    :
       'vram_freq'   :
       'power_limit' :
```

To have multiple GPU profiles, create new hash entries incrementing the id each time.

## API

See [REFERENCE.md](REFERENCE.md)

## Tips

* Bitcoin: [bc1qvuf05a6fvsezn5449snnnqud2ul3eu0n50jue2](bitcoin:BC1QVUF05A6FVSEZN5449SNNNQUD2UL3EU0N50JUE2?label=MonkyGames%20Donation)
* Ethereum: [0xDE5520EDa276F910206A98d2b9A70431CeC8AB5F](https://etherscan.io/address/0xDE5520EDa276F910206A98d2b9A70431CeC8AB5F)
