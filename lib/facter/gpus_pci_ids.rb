Facter.add(:gpus_pci_ids) do
  setcode do
    pci_id_string = Facter::Util::Resolution.exec("lspci | grep NVIDIA | grep VGA | cut -f1 -d ' ' | cut -f1 -d '.'")
    # ==========
    # For Testing purposes using vagrant + virtualbox
    # pci_id_string = "02:00\n03:00"
    # ==========
    pci_ids = pci_id_string.split("\n")
    pci_ids
  end
end
